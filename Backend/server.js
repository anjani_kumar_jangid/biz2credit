//mongo connection
var mongoose = require('mongoose');
mongoose.Promise=global.Promise
url='mongodb://localhost:27017/employee';
mongoose.connect(url,
    { useNewUrlParser: true, useUnifiedTopology: true }
)
console.log('mongo has been connect')



//import express
const express = require('express');
const path = require('path');
const app = express();
const session = require('express-session')


app.use(express.json());


//initialize session
app.use(session({
    secret: 'abcdef',
    proxy: true,
    resave: true,
    saveUninitialized: true,
     cookie: {
        maxAge: 24 * 60 * 60 * 1000 
    },
}));

//server is running on port 3001
app.listen(3001,()=> console.log('server is running on port ' , 3001))

    
//import scheema
require('./scheema/users.js')

//Models
const User=mongoose.model('User')
const Login=mongoose.model('login_Model')


//create user
app.post('/api/v1/session',async(req,res)=>{
    
  var u_username = req.body.username;
  var u_password = req.body.password;

    const data=await User.findOne({username:u_username,
        password:u_password})
        if (!data){
            res.send({
                message:'something went wrong'
            })
        } else {
                   
            if (!req.session.username){
                   req.session.username=req.body.username 
                   
            }
             console.log('req.session is this :'+ req.session.username) 
            res.sendStatus(200)
        }
   })


//user creation
app.post('/api/v1/users',async(req,res)=>{
    //get all the emails of all users
    var emails = await User.find({}).distinct('email')

    //if email exist in database
    if(emails.includes(req.body.email)){
      res.sendStatus(401).send({message: 'This email has already taken!'})
    }
    var user = await User.findOne({username:req.session.username})
    const recent_id = await User.find().sort({ _id: -1 }).limit(1)
    const next_id = recent_id[0] ? recent_id[0]._id + 1 : 1; 
    const obj = new User();
    obj._id = next_id
    obj.username = req.body.username;
    obj.email = req.body.email;
    obj.password = req.body.password;
    obj.mobile_number = req.body.mobile_number
    obj.created_by = user ? user._id :null
    obj.save((err) =>{
    if(err)
      throw err
    else{
        res.sendStatus(200);
       }
    })
     
})

//get all users
app.get('/api/v1/users',async(req,res) => {
  console.log('inside get request')
  const user = await User.findOne({username:req.session.username})
  const users = await User.find({created_by:user._id})
  res.send(users)
})

//get only one user for update
app.get('/user/:id',async(req,res) => {
  var user = await User.findOne({_id:req.params.id})
  res.send(user)
})

//delete user
app.delete('/api/v1/users/:id',async(req,res)=>{
  var removeQuery = User.deleteOne({_id : req.params.id});
  removeQuery.exec((err) =>{
    if(err){
      sendStatus(401);
    }else{
      
      res.sendStatus(200);
    }
  });
})


//update user data
app.put('/api/v1/users/:id',async(req,res)=>{
 
  var user = {
    username:req.body.username,
    password:req.body.password,
    email:req.body.email,
    mobile_number:req.body.mobile_number
  }
  User.updateOne({_id:req.params.id},user, function(err, hero) {
        if(err) {
            res.json({
                error : err
            })
        }else{
          res.sendStatus(200)
        }

    })
})

//on logout
app.delete('/api/v1/session',async(req,res)=>{
    const sess=req.session;
    var data = {
        "Data":""
    };
    
    sess.destroy(function(err) {
        if(err){
         
            data["Data"] = 'Error destroying session';
            res.json(data);
        }else{
        
            data["Data"] = 'Session destroy successfully';
            res.json(data);
         }
    });
})


//get session
app.get('/get_session',async(req,res)=>{
  if(req.session.username){
    res.sendStatus(200)
  }
  else{
    res.sendStatus(401)
  }
})

