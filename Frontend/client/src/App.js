import React from 'react';
import './App.css';
import { Suspense, lazy } from 'react';
const Authentication = lazy(() => import('./Authentication'));
const Main = lazy(() => import('./Main'));

class  App extends React.Component{
   constructor(props) {
        super(props);
        this.state={
          isauth:false
        }
      }


componentDidMount(){
  fetch('/get_session').then(response=>{

    if(response.ok){
      this.setState({
          isauth:true
        })
      }
    
  })
}

render(){
   if (!this.state.isauth) {
    
            return (<div><Suspense fallback="Loading....."><Authentication /></Suspense></div>)
        } else {
   
            return (<div><Suspense fallback="Loading....."><Main /></Suspense></div>);
        }
    }

}
    
  

export default App;
