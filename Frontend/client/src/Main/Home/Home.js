import React from 'react';
import {Redirect} from 'react-router-dom'
import { withRouter } from 'react-router-dom';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from "react-bootstrap/Card";
import ListGroupItem from "react-bootstrap/ListGroupItem"
import ListGroup from "react-bootstrap/ListGroup"
import {Row,Col,Container,Button} from "react-bootstrap"
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css'
import './Home.css'
class Home extends React.Component{
	constructor(){

		super()
		this.state={
			isLogin:true,
			users:[]
		}
	}

logout=()=>{
	const requestOptions = {
        method: 'DELETE',
        headers : { 
        'Content-Type': 'application/json'
        }
       };
	fetch('/api/v1/session',requestOptions).then(response=>{
		if(response.ok){
			localStorage.removeItem('username');
			this.setState({
				isLogin:false
			})
		  }
		})
	}

delete=(user_id)=>{
	const requestOptions = {
        method: 'DELETE',
        headers : { 
        'Content-Type': 'application/json'
        }
       };
	fetch('/api/v1/users/'+user_id,requestOptions).then(response=>{
		if(response.ok){
			var index=-1
			for (var i = this.state.users.length - 1; i >= 0; i--) {
				if(this.state.users[i]._id==user_id){
					index=i
					break
				}
			}
			if(index>0){
				this.state.users.splice(index)

			}
			this.setState({
				users:this.state.users
			})

		}
	})
 }



componentDidMount(){
	
	Promise.all([
            
            fetch('/get_session').then(response=>response),
            fetch('/api/v1/users').then(response=>response.json())
           
            ]).then(data=>{
            	var response = data[0]
            	var users = data[1]
            	
        		this.setState({
                	users:users
                })

            })
		}

render(){
	
	var token = localStorage.getItem('username')
	
	if(!token){
		return <Redirect to='/signin' />
	}else{
		return <div>

					  <div>
					  	<h1 id="heading">Home Page </h1>
					  </div>

					  <div>	
						  <Row className="row">
						    <Col sm={2} ></Col>
						     <Col sm={6} className="first-col">List of user</Col>
						      <Col sm={1} ></Col>
						        <Col sm={2} className="second-col">
							    	<Link to="/addUser" >
							    		<div id="addUser"> Create New User</div>
							    	</Link>
						       </Col>
						     <Col sm={1}></Col>
						  </Row>
					  </div>
  						
					  <Container>
						<div>
							<Row>

								{this.state.users.map(user =>(
					
									<Col sm={3} xs={12}>
										<Card >
										  	<ListGroup className="list-group-flush">
												<ListGroupItem className="list-item">Name:{user.username}</ListGroupItem>
											    <ListGroupItem className="list-item">Email:{user.email}</ListGroupItem>
											    <ListGroupItem className="list-item">Phone Number:{user.mobile_number}</ListGroupItem>
										  	</ListGroup>
										  	<Card.Body>
											    <Button id="btn1"><Link to={"/update/"+user._id}><div id="addUser">Edit</div></Link></Button>

											   <Popup trigger={<Button id="btn2" variant="danger">delete</Button>} position="right center">
											   		<div>are you sure  </div>
											   		<div onClick={(e)=>{this.delete(user._id)}}>yes</div>
											   </Popup>
											    
										  	</Card.Body>
										</Card>
									</Col>
									
						        ))}
				     		</Row>
				   		</div>
				     </Container>

				   	<br></br>
				   	
				    <Button id="logout" onClick={this.logout}>Logout</Button>
			</div>

	  }
	}
}
export default Home;
